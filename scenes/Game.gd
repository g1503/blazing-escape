extends Node2D

onready var gameover_delay = $GameOverDelay
onready var play_btn = $UI/Control/Play
onready var fuel_bar = $UI/Control/ProgressBar
onready var points_label = $UI/Control/Points

var game_started = false

func _ready():
	
	Globals.fuel = 64
	fuel_bar.max_value = Globals.fuel
	fuel_bar.value = Globals.fuel
	get_tree().paused = true


func _physics_process(delta):
	
	points_label.text = str(int(Globals.points))


func game_over():
	
	pause()


func pause():
	
	match get_tree().paused:
		true: get_tree().paused = false
		false: get_tree().paused = true
		
	match play_btn.visible:
		true: play_btn.hide()
		false: play_btn.show()


func _on_FireBall_player_dead():
	
	gameover_delay.start()


func _on_GameOverDelay_timeout():
	
	game_over()


func _on_Play_pressed():
	
	match game_started:
		true:
			get_tree().reload_current_scene()
		false:
			play_btn.text = "Restart"
			pause()
			game_started = true
